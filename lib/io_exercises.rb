# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  puts 'guess a number'
  num = rand(1..100)
  guesses = []
  guess = 0
  until guess == num
    guess = gets.chomp.to_i
    puts "#{guess}"
    if guess > num
      puts 'too high'
    elsif guess < num
      puts 'too low'
    end
    guesses << guess
  end
  puts guesses.length
end

def file_shuffler
  puts 'enter a filename:'
  filename = gets.chomp
  contents = File.readlines(filename)
  contents.shuffle!
  f = File.open("#{filename}--shuffled.txt", 'w')
  f.puts contents
end

file_shuffler
